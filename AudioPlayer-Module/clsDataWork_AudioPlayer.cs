﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Ontology_Module;

namespace AudioPlayer_Module
{
    public class clsDataWork_AudioPlayer
    {
        private clsLocalConfig objLocalConfig;

        private List<clsOntologyItem> OList_MediaItems;

        public List<clsReferencedMediaItem> OList_MultimediaItemsRef { get; set; }

        public clsMultimediaItem MultiMediaItem_Current { get; set; }

        public List<clsMultimediaItem> OList_MultiMediaItems { get; set; }

        private OntologyModDBConnector objDBLevel_MediaItems;
        private OntologyModDBConnector objDBLevel_FilesOfMediaItems;
        private OntologyModDBConnector objDBLevel_Created;
        private OntologyModDBConnector objDBLevel_DataTypes;
        private OntologyModDBConnector objDBLevel_RelationTypes;
        private OntologyModDBConnector objDBLevel_Classes;
        private OntologyModDBConnector objDBLevel_RefItems;

        public clsOntologyItem GetData_MediaItemsAndRefs()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();
            

            OList_MultiMediaItems = new List<clsMultimediaItem>();

            objOItem_Result = GetSubData_001_MediaItemsOfRef();

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
             
             
                        OList_MultimediaItemsRef = (from objMultimediaItem in objDBLevel_MediaItems.ObjectRels
                                                    join objClass in objDBLevel_Classes.Classes1 on objMultimediaItem.ID_Parent_Other equals objClass.GUID into objClasses
                                                    from objClass in objClasses.DefaultIfEmpty()
                                                    join objDataType in objDBLevel_DataTypes.DataTypes on objMultimediaItem.ID_Parent_Other equals objDataType.GUID into objDataTypes
                                                    from objDataType in objDataTypes.DefaultIfEmpty()
                                                     select new clsReferencedMediaItem { ID_Item = objMultimediaItem.ID_Object,
                                                         Name_Item = objMultimediaItem.Name_Object,
                                                         ID_Parent_Item = objMultimediaItem.ID_Parent_Object,
                                                         OrderID = objMultimediaItem.OrderID,
                                                         CountBookmark = 0,
                                                     ID_Ref = objMultimediaItem.ID_Other,
                                                     Name_Ref = objMultimediaItem.Name_Other,
                                                     ID_Parent_Ref = objMultimediaItem.ID_Parent_Other,
                                                     Name_Parent_Ref = objClass != null ? objClass.Name : objDataType != null ? objDataType.Name : null,
                                                     Ontology = objMultimediaItem.Ontology}).ToList();

             
            }

            return objOItem_Result;
        }

        public clsOntologyItem GetData_MediaItem(List<clsOntologyItem> OList_MediaItems)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            this.OList_MediaItems = OList_MediaItems;

            objOItem_Result = GetSubData_002_FilesOfMediaItems();

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = GetSubData_003_CreatedAttribute();

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    OList_MultiMediaItems = (from objMediaItem in OList_MediaItems
                                             join objFile in objDBLevel_FilesOfMediaItems.ObjectRels.Where(f => f.Name_Other.ToLower().EndsWith(objLocalConfig.OItem_object_aiff.Name.ToLower()) ||
                                                                                                                     f.Name_Other.ToLower().EndsWith(objLocalConfig.OItem_object_mp3.Name.ToLower()) ||
                                                                                                                     f.Name_Other.ToLower().EndsWith(objLocalConfig.OItem_object_wav.Name.ToLower())).ToList() on objMediaItem.GUID equals objFile.ID_Object
                                             join objCreated in objDBLevel_Created.ObjAtts on objFile.ID_Other equals objCreated.ID_Object into objCreatedList
                                             from objCreated in objCreatedList.DefaultIfEmpty()
                                             select new clsMultimediaItem
                                             {
                                                 ID_Item = objMediaItem.GUID,
                                                 Name_Item = objMediaItem.Name,
                                                 ID_Parent_Item = objMediaItem.GUID_Parent,
                                                 ID_File = objFile.ID_Other,
                                                 Name_File = objFile.Name_Other,
                                                 ID_Parent_File = objFile.ID_Parent_Other,
                                                 OACreate = objCreated,
                                                 OrderID = 0,
                                                 CountBookmark = 0
                                             }).ToList();
                }
            }
            
            return objOItem_Result;
        }

        public List<clsOntologyItem> GetMediaItemsRelated(List<clsOntologyItem> OList_Ref)
        {
            var searchMediaItems = OList_Ref.Select(mi => new clsObjectRel
            {
                ID_Object = mi.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_media_item.GUID
            }).ToList();

            var olist_MediaItems = new List<clsOntologyItem>();

            var objOItem_Result = objDBLevel_RefItems.GetDataObjectRel(searchMediaItems, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                olist_MediaItems.AddRange(objDBLevel_RefItems.ObjectRels.Select(refitem => new clsOntologyItem
                {
                    GUID = refitem.ID_Other,
                    Name = refitem.Name_Other,
                    GUID_Parent = refitem.ID_Parent_Other,
                    Type = objLocalConfig.Globals.Type_Object
                }));

                searchMediaItems = OList_Ref.Select(mi => new clsObjectRel
                {
                    ID_Parent_Object = objLocalConfig.OItem_class_media_item.GUID,
                    ID_Other = mi.GUID
                }).ToList();

                objOItem_Result = objDBLevel_RefItems.GetDataObjectRel(searchMediaItems, doIds: false);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    olist_MediaItems.AddRange(objDBLevel_RefItems.ObjectRels.Select(refitem => new clsOntologyItem
                    {
                        GUID = refitem.ID_Object,
                        Name = refitem.Name_Object,
                        GUID_Parent = refitem.ID_Parent_Object,
                        Type = objLocalConfig.Globals.Type_Object
                    }));
                }
                else
                {
                    olist_MediaItems = null;
                }

            }
            else
            {
                olist_MediaItems = null;
            }

            return olist_MediaItems;
        }

        private clsOntologyItem GetSubData_001_MediaItemsOfRef()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objORel_MediaItemsOfRef = new List<clsObjectRel> { new clsObjectRel { ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Object = objLocalConfig.OItem_class_media_item.GUID } };

            objOItem_Result = objDBLevel_MediaItems.GetDataObjectRel(objORel_MediaItemsOfRef, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = objDBLevel_Classes.GetDataClasses();

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    objOItem_Result = objDBLevel_DataTypes.GetDataDataTypes();

                    
                }
            }
            
            return objOItem_Result;
        }

        private clsOntologyItem GetSubData_002_FilesOfMediaItems()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            List<clsObjectRel> objORel_MediaItems_To_Files;

            if (OList_MediaItems != null)
            {
                objORel_MediaItems_To_Files = OList_MediaItems.Select(mi => new clsObjectRel { ID_Object = mi.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = objLocalConfig.objLocalConfig_File.OItem_Type_File.GUID }).ToList();

                
            }
            else
            {
                objORel_MediaItems_To_Files = objDBLevel_MediaItems.ObjectRels.Select(mi => new clsObjectRel {
                    ID_Object = mi.ID_Object,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belonging_source.GUID,
                    ID_Parent_Other = objLocalConfig.objLocalConfig_File.OItem_Type_File.GUID
                } ).ToList();



            }

            if (objORel_MediaItems_To_Files.Any())
            {
                objOItem_Result = objDBLevel_FilesOfMediaItems.GetDataObjectRel(objORel_MediaItems_To_Files, doIds: false);
            }
            else
            {
                objDBLevel_FilesOfMediaItems.ObjectRels.Clear();
            }
            
            return objOItem_Result;
        }

        private clsOntologyItem GetSubData_003_CreatedAttribute()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();


            var objORel_File_To_Attribute = objDBLevel_FilesOfMediaItems.ObjectRels.Select(f => new clsObjectAtt
            {
                ID_Object = f.ID_Other,
                ID_AttributeType = objLocalConfig.objLocalConfig_File.OItem_Attribute_Datetimestamp__Create_.GUID
            }).ToList();

            if (objORel_File_To_Attribute.Any())
            {
                objOItem_Result = objDBLevel_Created.GetDataObjectAtt(objORel_File_To_Attribute, doIds: false);

            }
            else
            {
                objDBLevel_Created.ObjAtts.Clear();
            }
            

            return objOItem_Result;
        }

        public clsDataWork_AudioPlayer(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        private void Initialize()
        {
            objDBLevel_FilesOfMediaItems = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_MediaItems = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Created = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_DataTypes = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Classes = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RelationTypes = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RefItems = new OntologyModDBConnector(objLocalConfig.Globals);
        }
    }
}
